from django.urls import path
from .views import DemoView, RegisterView, LogOutView, LoginView


urlpatterns = [
    path('', DemoView, name='demo_url'),
    path('register/', RegisterView, name='register_url'),
    path('logout', LogOutView, name='logout_url'),
    path('login/', LoginView, name='login_url'),
]