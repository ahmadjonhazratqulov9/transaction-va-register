# Generated by Django 3.1.7 on 2021-05-12 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('wallet', '0005_auto_20210512_1812'),
    ]

    operations = [
        migrations.RemoveConstraint(
            model_name='wallet',
            name='zero',
        ),
        migrations.AddConstraint(
            model_name='wallet',
            constraint=models.CheckConstraint(check=models.Q(balance__gte=0), name='more_than_zero'),
        ),
    ]
