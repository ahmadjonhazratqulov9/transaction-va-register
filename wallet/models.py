from django.db import models
from uauth.models import User
from django.db.models.constraints import CheckConstraint
from django.db.models import Q


class Wallet(models.Model):
    wallet_id = models.CharField(max_length=16, unique=True)
    balance = models.IntegerField(default=10000)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user')

    def __str__(self):
        return f"{self.user.phone_number}   |   {self.wallet_id}  |  {self.balance} so`m"

    class Meta:
        constraints = [CheckConstraint(check=Q(balance__gte=0), name='more_than_zero')]



class AppTransaction(models.Model):
    from_user = models.ForeignKey(User, on_delete=models.CASCADE, db_constraint= True, related_name='from_user')
    to_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='to_user')
    amount = models.IntegerField()
    create_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.from_user} > {self.to_user} | {self.amount}"



