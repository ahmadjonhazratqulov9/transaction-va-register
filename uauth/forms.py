from django import forms


class UserRegisterForm(forms.Form):
    phone_number = forms.CharField(max_length=255)
    password1 = forms.CharField(max_length=255)
    password2 = forms.CharField(max_length=255)


class UserLoginForm(forms.Form):
    phone_number = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255)