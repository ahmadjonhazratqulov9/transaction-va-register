from django.shortcuts import render, redirect
from uauth.forms import UserRegisterForm, UserLoginForm
from uauth.models import User
from wallet.models import Wallet
from wallet.utils import generate_wallet_id
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.decorators import login_required


@login_required(login_url='register/')
def DemoView(request):
    print(request.user)
    return render(request,'index.html')

def LogOutView(request):
    logout(request)
    return redirect('login_url')


def RegisterView(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['password1'] == form.cleaned_data['password2']:
                try:
                    user = User(phone_number=form.cleaned_data['phone_number'])
                    user.set_password(form.cleaned_data['password1'])
                    user.save()

                    """ ruyxatdan utgan userga wallet id yaratish """
                    while True:
                        wallet_id = generate_wallet_id()
                        if Wallet.objects.filter(wallet_id=wallet_id).count() == 0:
                            Wallet.objects.create(wallet_id=wallet_id, user=user)
                            break

                    login(request, user)
                    return redirect('demo_url')
                except Exception as e:
                    print(e)

    return render(request, 'register.html')


def LoginView(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            user = authenticate(request, **form.cleaned_data)
            if user is not None:
                login(request, user)
                return redirect('demo_url')

    return render(request, 'login.html')